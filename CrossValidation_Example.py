# Christian Arnold, ICC, Durham University, 08/2021, 
# based on an older version of 23/04/2018, B. M. Giblin, PhD Student, Edinburgh
import numpy as np
import sys
import os
# For doing Gaussian processes, PCAs and plotting output
from GPR_Emulator import GPR_Emu


# specify directory and filename for the output files 
savedir = 'CrossValidation_Output/CEN_output/'
savename = 'TrainN50_PCATrue_ScaleNodesTrue'

# load the training node parameters from a txt file
node_file = 'FoR_MatterPowerSpec_TrainingData/nodes_learn_file_boxes.txt'
Train_Nodes = np.loadtxt(node_file)

# load the training powerspec response data from a binary .npy file containing the k-vector (0-element) and the B(k) (1-element)
training_data_file = 'FoR_MatterPowerSpec_TrainingData/powerspec_learn_data_boxes.npy'
tmp = np.load(training_data_file)
Train_x = tmp[0, 0, :] #k-vector
Train_Pred = tmp[1] #B(k)

# load the error of the training data from a similar file
training_error_file = 'FoR_MatterPowerSpec_TrainingData/powerspec_learn_error_boxes.npy'
tmp = np.load(training_error_file)
Train_ErrPred = tmp[1]


save_file_base = 'EmulatorState/EmulatorCVState' #only to save the internal state of the emulator

# Do cross-validation
print(Train_Pred.shape)
GPR_Class = GPR_Emu(save_file_base, Train_Pred.shape[1], use_train_err = True)

n_restarts_optimizer = 20 # number of independent training run restarts
HPs = np.ones(len(Train_Nodes[0]) + 1) # initial guess for the hyper parameters - can be set to 1 if no good values are known
CV_Pred, CV_err = GPR_Class.Cross_Validation(HPs, Train_Nodes, Train_Pred, Train_ErrPred, n_restarts_optimizer)


# Save pickled CV predictions & accuracies plus a datafile containing corresponding x-array     
np.save(savedir + 'CVPred_' + savename, CV_Pred)
np.save(savedir + 'CVAcc_'  + savename, CV_Pred/Train_Pred)
np.savetxt(savedir + 'xArray_' + savename + '.dat', Train_x)























