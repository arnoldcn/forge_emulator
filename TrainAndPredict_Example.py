# Christian Arnold, ICC, Durham University, 08/2021, 
import numpy as np
import matplotlib
import sys
import os
# For doing Gaussian processes
from FORGE_emulator import FORGE

#create an emulator instance; 
#it's recommended to do this once at the start as multiple 
#instances might lead to duplications in training work
forge_emulator = FORGE()

#now we want to make predictions for the following cosmology
omega_m = 0.3
h = 0.72
fR0 = 5 #this corresponds to f_R0 = -1e-5
sigma8 = 0.8

#and we have a list of redshifts we want to make predictions for
redshifts = [0.0, 0.1, 0.12, 0.25]    

#plot a figure for the results
fig, ax = matplotlib.pyplot.subplots(1,1)

#create a dict to store the results for B(k) and P(k)
Bk = {}
Pk = {}

for redshift in redshifts:
    #Make a prediction for the B(k) = P(k) / P(k)_Halofit^LCDM and its error.
    #If the emulator is not yet trained, it will do the training. Once trained,
    #the emulator is very fast in making predictions for different cosmologies.
    #If this is requested for one of the redshifts the emulator was trained on 
    #it will return the emulator prediction directly. For all other redshifts it
    #will interpolate between the tqo nearest training redshifts.
    Bk[redshift], error = forge_emulator.predict_Bk(redshift, omega_m, h, fR0, sigma8)

    #Make a prediction for the P(k) using the B(k) emulator prediction (see above) and 
    #Halofit/CAMB. Also returns the error of P(k).
    Pk[redshift], error = forge_emulator.predict_Pk(redshift, omega_m, h, fR0, sigma8)
    
    #plot the prediction
    ax.plot(forge_emulator.k, Bk[redshift], ls = '-', label = 'z=%.2f'%redshift)



#some more plotting
ax.set_xscale('log')
ax.set_xlabel(r'$k [Mpc/h]$')
ax.set_ylabel(r'$B(k)$')
ax.legend()
fig.show()






















